import React from "react";
import { useColorScheme, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {
    QueryClient,
    QueryClientProvider,
  } from '@tanstack/react-query'
import { darkTheme, lightTheme } from "./src/themes";
import TabNavigation from "./src/components/navigation/TabNavigation";

const queryClient = new QueryClient()

export default function App() {
  const scheme = useColorScheme();
  const theme = (scheme === 'dark') ? darkTheme : lightTheme
  return (
    <NavigationContainer  theme={theme}>
    
      <StatusBar
          backgroundColor={theme.colors.primary}
      />
      <QueryClientProvider client={queryClient}>
        <TabNavigation />
      </QueryClientProvider>
    </NavigationContainer>
  );
}