interface Dog {
  id: string;
  name: string;
  temperament: string;
  life_span: string;
  alt_names: string;
  wikipedia_url: string;
  origin: string;
  weight: Weight;
  height: Height;
}

interface Measure {
  imperial: string;
  metric: string;
}

interface Weight extends Measure {
  // nothing more for now
}
interface Height extends Measure {
  // nothing more for now
}

interface DogImage {
  id: string;
  url: string;
}

export { Dog, DogImage };
