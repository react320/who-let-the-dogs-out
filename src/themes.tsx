import { DarkTheme, DefaultTheme } from '@react-navigation/native';

const DefaultFontsTheme = {
  fontsizes: {
    s: 12,
    m: 18,
    l: 24,
    xl: 32,
    xxl: 36
  }
};

const DefaultSpacesTheme = {
  spaces: {
    xs: 8,
    s: 12,
    m: 16,
    l: 32,
    xl: 36
  }
};

const lightTheme = {
  ...DefaultTheme,
  ...DefaultFontsTheme,
  ...DefaultSpacesTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#ffb142',

    background: 'rgb(242, 242, 242)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',

    inactive: '#b2bec3'
  }
};

const darkTheme = {
  ...DarkTheme,
  ...DefaultFontsTheme,
  ...DefaultSpacesTheme,
  colors: {
    ...DarkTheme.colors,
    background: '#16161D',
    primary: '#ffb142',

    text: 'rgb(242, 242, 242)',
    card: '#16161D',

    inactive: '#b2bec3'
  }
};

export { lightTheme, darkTheme };
