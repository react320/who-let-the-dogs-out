import React from 'react';
import DogHouseHeader from '../components/doghouse/DogHouseHeader';
import DogHouseSearchbar from '../components/doghouse/DogHouseSearchbar';
import DogHouseResult from '../components/doghouse/DogHouseResult';

export default function ({ navigation }) {
  const [search, setSearch] = React.useState(''); 

  return (
    <>
      <DogHouseHeader />
      <DogHouseSearchbar emailHandler={setSearch} />
      <DogHouseResult search={search} />
    </>
  );
}
