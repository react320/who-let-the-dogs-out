import { useTheme } from '@react-navigation/native';
import { Button } from '@rneui/base';
import { Text } from 'react-native';

export default function ({ onRefresh }) {
  const { colors } = useTheme();

  return (
    <>
      <Text>An error has occured :(</Text>
      <Button
        onPress={onRefresh}
        title="Retry"
        color={colors.primary}
        accessibilityLabel="Try to search again"
      />
    </>
  );
}
