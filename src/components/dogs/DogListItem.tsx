import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { IconButton } from 'react-native-paper';
import { useTheme } from '@react-navigation/native';
import { useNavigation } from '@react-navigation/native';

function DogListItem({ id, name }) {
  const nav = useNavigation();
  const { colors } = useTheme();
  const styles = getStyle(useTheme());

  return (
    <View style={styles.ItemLayout}>
      <Text style={styles.ItemTitle}>{name}</Text>
      <IconButton
        icon="chevron-right"
        containerColor={colors.primary}
        iconColor="white"
        size={20}
        mode="contained"
        onPress={() =>
          nav.navigate('Detail' as never, { name: name, id: id } as never)
        }
      />
    </View>
  );
}

const getStyle = (theme) =>
  StyleSheet.create({
    ItemLayout: {
      display: 'flex',
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
      fontSize: theme.fontsizes.m,
      paddingVertical: theme.spaces.s
    },
    ItemTitle: {
      fontSize: theme.fontsizes.l,
      color: theme.colors.text
    }
  });

export default DogListItem;
