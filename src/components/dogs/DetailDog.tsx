import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { ScrollView, Text, Image, StyleSheet } from 'react-native';
import { getDog, getBreedImages } from '../../api/DogsApi';
import { useTheme } from '@react-navigation/native';
import useGetDog from '../../api/useGetDog';
import useGetDogImage from '../../api/useGetDogImage';

function DetailDog({ route }) {
  const styles = getStyle(useTheme());

  const { id, name } = route.params;
  const queryDog = useGetDog(name);
  const queryImage = useGetDogImage(id);

  return (
    <ScrollView style={styles.Layout} contentContainerStyle={{ flexGrow: 1 }}>
      <Text style={styles.Title}>{queryDog.data?.name}</Text>
      <Image style={styles.Image} source={{ uri: queryImage?.data?.url }} />
      <Text style={styles.Info}>Origin : {queryDog.data?.origin}</Text>
      <Text style={styles.Info}>Life span : {queryDog.data?.life_span}</Text>
    </ScrollView>
  );
}

const getStyle = (theme) =>
  StyleSheet.create({
    Layout: {
      position: 'relative',
      flex: 1
    },
    Title: {
      textAlign: 'center',
      fontSize: theme.fontsizes.xxl,
      fontWeight: '500',
      color: theme.colors.text
    },
    Image: {
      width: '100%',
      height: 300,
      resizeMode: 'cover'
    },
    Info: {
      fontSize: theme.fontsizes.m,
      fontWeight: '300',
      paddingHorizontal: theme.spaces.m,
      paddingVertical: theme.spaces.xs,
      color: theme.colors.text
    }
  });

export default DetailDog;
