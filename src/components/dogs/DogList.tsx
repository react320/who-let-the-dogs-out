import { useTheme } from '@react-navigation/native';
import { FlatList, RefreshControl, StyleSheet } from 'react-native';
import DogListItem from './DogListItem';

export default function ({ data, refreshing, onRefresh }) {
  const styles = getStyle(useTheme());

  return (
    <FlatList
      contentContainerStyle={styles.List}
      data={data}
      renderItem={({ item }) => <DogListItem id={item.id} name={item.name} />}
      keyExtractor={(item) => item.id}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    />
  );
}

const getStyle = (theme) =>
  StyleSheet.create({
    List: {
      paddingHorizontal: theme.spaces.xl,
      paddingVertical: theme.spaces.m
    }
  });
