import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from '@rneui/themed';
import { useColorScheme } from 'react-native';
import { darkTheme, lightTheme } from '../../themes';
import DogStackScreen from '../../stacks/DogStack';
import ComingSoonPage from '../../pages/ComingSoon';

const Tab = createBottomTabNavigator();

function TabNavigation() {
  const scheme = useColorScheme();
  const theme = scheme === 'dark' ? darkTheme : lightTheme;
  return (
    <>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarActiveTintColor: theme.colors.primary,
          tabBarInactiveTintColor: theme.colors.inactive,

          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            switch (route.name) {
              case 'Doghouse':
                iconName = 'dog';
                break;
              case 'Food Market':
                iconName = 'hotdog';
                break;
            }
            // You can return any component that you like here!
            return (
              <Icon
                name={iconName}
                type="font-awesome-5"
                color={focused ? theme.colors.primary : theme.colors.inactive}
              />
            );
          }
        })}
      >
        <Tab.Screen name="Doghouse" component={DogStackScreen} />
        <Tab.Screen name="Food Market" component={ComingSoonPage} />
      </Tab.Navigator>
    </>
  );
}

export default TabNavigation;
