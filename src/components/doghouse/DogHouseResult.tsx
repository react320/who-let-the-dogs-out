import React from 'react';
import { Text } from 'react-native';
import useGetDogs from '../../api/useGetDogs';
import useHomeSearch from '../../api/useHomeSearch';
import useSearchDogs from '../../api/useSearchDogs';
import DogListError from '../DogListError';
import DogList from '../dogs/DogList';

export default function ({ search }) {
  const [refreshing, setRefreshing] = React.useState(false);

  const query = useHomeSearch(search)

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    query.refetch().then(() => setRefreshing(false));
  }, []);

  if (query.isLoading) {
    return <Text>Recherche en cours</Text>;
  } else if (query.isError) {
    return <DogListError onRefresh={onRefresh} />;
  } else {
    return (
      <DogList
        data={query.data}
        refreshing={refreshing}
        onRefresh={onRefresh}
      />
    );
  }
}
