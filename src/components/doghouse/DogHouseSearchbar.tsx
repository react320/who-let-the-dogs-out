import { useTheme } from '@react-navigation/native';
import { Text, View, Image, StyleSheet, TextInput } from 'react-native';

export default function ({ emailHandler }) {
  const styles = getStyle(useTheme());
  return (
    <View style={styles.SearchLayout}>
      <Text style={styles.SearchTitle}>Search a breed to get informations</Text>
      <TextInput style={styles.SearchInput} onChangeText={emailHandler} />
    </View>
  );
}

const getStyle = (theme) =>
  StyleSheet.create({
    SearchLayout: {
      backgroundColor: theme.colors.primary,
      marginTop: -27,
      zIndex: 1,
      paddingVertical: theme.spaces.m
    },
    SearchTitle: {
      color: 'white',
      textAlign: 'center',
      fontSize: theme.fontsizes.l
    },
    SearchInput: {
      backgroundColor: 'white',
      fontSize: theme.fontsizes.m,
      paddingHorizontal: theme.spaces.m,
      paddingVertical: theme.spaces.xs,
      marginHorizontal: theme.spaces.l,
      borderRadius: 10,
      marginTop: theme.spaces.m,
      color: 'black'
    }
  });
