import { useTheme } from '@react-navigation/native';
import { Text, View, Image, StyleSheet } from 'react-native';

export default function () {
  const styles = getStyle(useTheme());
  return (
    <View style={styles.Header}>
      <View style={styles.PageTitleLayout}>
        <Text style={styles.PageTitle}>Find your next {'\n'}best friend !</Text>
      </View>
      <View>
        <Image
          style={styles.PageHiDogDecoration}
          source={require('../../../assets/decoration/hidog.png')}
        />
      </View>
    </View>
  );
}

const getStyle = (theme) =>
  StyleSheet.create({
    Header: {
      display: 'flex',
      flexDirection: 'row',
      height: 200,
      zIndex: 5,
      color: theme.colors.text
    },
    PageTitleLayout: {
      width: '55%'
    },
    PageTitle: {
      fontSize: theme.fontsizes.xl,
      textAlign: 'left',
      fontWeight: '800',
      paddingTop: theme.spaces.xl,
      paddingLeft: theme.spaces.m,
      color: theme.colors.text
    },
    PageHiDogDecoration: {
      width: 300,
      height: 200,
      flex: 1,
      resizeMode: 'contain',
      transform: [{ translateX: -50 }]
    }
  });
