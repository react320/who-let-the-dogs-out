import { useQuery } from '@tanstack/react-query';
import { DogImage } from '../types/Dogs';
import dogClient from './clients/dogClient';

const getBreedImage = async (id: number): Promise<DogImage> => {
  const conf = {
    params: {
      breed_id: id,
      limit: 1
    }
  };
  const { data } = await dogClient.get('images/search', conf);
  return data[0] ?? null;
};

function useGetDogImage(name) {
  const { data, isLoading, isError, refetch } = useQuery({
    queryKey: ['dog-image'],
    queryFn: () => getBreedImage(name)
  });

  return { data, isLoading, isError, refetch };
}

export default useGetDogImage;
