import { useQuery } from '@tanstack/react-query';
import { Dog } from '../types/Dogs';
import dogClient from './clients/dogClient';

const searchDogs = async (search: string): Promise<Dog[]> => {
  if (search.toLowerCase() == 'error') throw new Error();
  const { data } = await dogClient.get('breeds/search', {
    params: { q: search }
  });
  return data;
};

function useSearchDogs(search: string, enabled) {
  const { data, isLoading, isError, refetch } = useQuery({
    queryKey: ['dogs', search],
    queryFn: () => searchDogs(search),
    enabled: enabled
  });

  return { data, isLoading, isError, refetch };
}

export default useSearchDogs;
