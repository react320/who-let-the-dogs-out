import { useQuery } from '@tanstack/react-query';
import { Dog } from '../types/Dogs';
import dogClient from './clients/dogClient';

const getDogs = async (): Promise<Dog[]> => {
  const { data } = await dogClient.get('breeds');
  return data;
};

function useGetDogs(enabled) {
  const { data, isLoading, isError, refetch } = useQuery({
    queryKey: ['dogs'],
    queryFn: getDogs,
    enabled: enabled
  });

  return { data, isLoading, isError, refetch };
}

export default useGetDogs;
