import { useQuery } from '@tanstack/react-query';
import { Dog } from '../types/Dogs';
import dogClient from './clients/dogClient';

const getDog = async (name: string): Promise<Dog> => {
  const { data } = await dogClient.get('breeds/search', {
    params: { q: name }
  });
  return data[0];
};

function useGetDog(name) {
  const { data, isLoading, isError, refetch } = useQuery({
    queryKey: ['dog'],
    queryFn: () => getDog(name)
  });

  return { data, isLoading, isError, refetch };
}

export default useGetDog;
