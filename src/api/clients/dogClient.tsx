import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.thedogapi.com/v1/',
  timeout: 1000,
  headers: {
    'x-api-key':
      'live_uZV53qz3m2zvIgkrnYMmtDnZmxKj9cZY9m7WZz0HFGlwYWP0eEywdDnK6LDmkyo2'
  }
});

/*
const BASEAPI = 'https://api.thedogapi.com/v1/';

let config = {
  headers: {
    'x-api-key':
      'live_uZV53qz3m2zvIgkrnYMmtDnZmxKj9cZY9m7WZz0HFGlwYWP0eEywdDnK6LDmkyo2'
  },
  params: {}
};
*/
