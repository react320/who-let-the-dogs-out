import useGetDogs from "./useGetDogs";
import useSearchDogs from "./useSearchDogs";

function useHomeSearch(search: string) {
    
    const queryAll = useGetDogs(search.length === 0);
    const querySearch = useSearchDogs(search, search.length > 0);

    return (search.length === 0) ? queryAll : querySearch
}

export default useHomeSearch