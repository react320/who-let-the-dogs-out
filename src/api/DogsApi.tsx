import axios from 'axios';
import { Dog, DogImage } from '../types/Dogs';
import dogClient from './clients/dogClient';

const BASEAPI = 'https://api.thedogapi.com/v1/';

let config = {
  headers: {
    'x-api-key':
      'live_uZV53qz3m2zvIgkrnYMmtDnZmxKj9cZY9m7WZz0HFGlwYWP0eEywdDnK6LDmkyo2'
  },
  params: {}
};

const getDogs = async (): Promise<Dog[]> => {
  const { data } = await dogClient.get('breeds');
  return data;
};

const searchDogs = async (search: string): Promise<Dog[]> => {
  if (search.toLowerCase() == 'error') throw new Error();
  const { data } = await dogClient.get('breeds/search', {
    params: { q: search }
  });
  return data;
};

const getDog = async (name: string): Promise<Dog> => {
  const { data } = await axios.get('breeds/search', { params: { q: name } });
  return data[0];
};

const getBreedImages = async (id: number): Promise<DogImage> => {
  const conf = {
    params: {
      breed_id: id,
      limit: 1
    }
  };
  const { data } = await axios.get('images/search', conf);
  return data[0] ?? null;
};

export { getDogs, searchDogs, getDog, getBreedImages };
