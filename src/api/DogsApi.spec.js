import { getDogs, searchDogs } from "./DogsApi";

describe("Api function", () => {
    test("Api should return promise", async () => {
      expect.assertions(1);
      const data = await getDogs();
      expect(data.length).toBeGreaterThan(0)
    });
});