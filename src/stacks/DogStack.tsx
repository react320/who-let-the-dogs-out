import React from 'react';
import { QueryClient } from '@tanstack/react-query';
import { createStackNavigator } from '@react-navigation/stack';
import DetailDog from '../components/dogs/DetailDog';
import DogHouseScreen from '../screens/DogHouseScreen';

// Create a client
const queryClient = new QueryClient();

const DogStack = createStackNavigator();

function DogStackScreen() {
  return (
    <DogStack.Navigator>
      <DogStack.Screen
        name="List"
        component={DogHouseScreen}
        options={{ title: 'Discover' }}
      />
      <DogStack.Screen
        name="Detail"
        component={DetailDog}
        options={({ route }: any) => ({ title: route.params.name })}
      />
    </DogStack.Navigator>
  );
}

export default DogStackScreen;
