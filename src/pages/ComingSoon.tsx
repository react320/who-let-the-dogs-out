import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { useTheme } from '@react-navigation/native';
import { ScrollView, Text, Image, StyleSheet, View } from 'react-native';

function ComingSoonPage() {
  const styles = getStyle(useTheme());

  return (
    <View style={styles.Layout}>
      <Text style={styles.Text}>Coming Soon</Text>
    </View>
  );
}

const getStyle = (theme) =>
  StyleSheet.create({
    Layout: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%'
    },
    Text: {
      fontSize: theme.fontsizes.xxl,
      color: theme.colors.primary,
      fontWeight: 'bold'
    }
  });

export default ComingSoonPage;
